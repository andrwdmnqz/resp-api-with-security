package ua.kr.shokhirev.andrii.springrestapi.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import ua.kr.shokhirev.andrii.springrestapi.model.Author;
import ua.kr.shokhirev.andrii.springrestapi.model.dto.AuthorRespDTO;
import ua.kr.shokhirev.andrii.springrestapi.model.dto.BookReqDTO;
import ua.kr.shokhirev.andrii.springrestapi.model.dto.BookRespDTO;
import ua.kr.shokhirev.andrii.springrestapi.model.mappers.AuthorMapper;
import ua.kr.shokhirev.andrii.springrestapi.model.mappers.BookMapper;
import ua.kr.shokhirev.andrii.springrestapi.service.AuthorService;
import ua.kr.shokhirev.andrii.springrestapi.service.BookService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@ExtendWith(SpringExtension.class)
public class AuthorControllerTest {

    private static String END_POINT_PATH = "/authors";

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private AuthorMapper authorMapper;

    @Autowired
    private BookMapper bookMapper;

    @MockBean
    private AuthorService authorService;

    @Test
    public void testGetAuthorThatDoNotExistsReturn400BadRequest() throws Exception {
        Long authorId = 123L;
        String requestURI = END_POINT_PATH + "/" + authorId;

        mockMvc.perform(MockMvcRequestBuilders.get(requestURI).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andDo(print());
    }

    @Test
    public void testGetAuthorReturnOk200() throws Exception {

        Long bookId = 123L;
        String requestURI = END_POINT_PATH + "/" + bookId;

        Author author = Author.builder()
                .id(1L)
                .name("author name")
                .build();

        Optional<AuthorRespDTO> authorResponseDTOOptional = Optional.ofNullable(
                authorMapper.getAuthorResponseFromAuthor(author));

        when(authorService.getSingleAuthor(Mockito.any(Long.class))).thenReturn(authorResponseDTOOptional);

        mockMvc.perform(MockMvcRequestBuilders.get(requestURI).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(print());
    }

    @Test
    public void testGetAllAuthorsReturn204NoContent() throws Exception {

        List<AuthorRespDTO> emptyAuthorList = new ArrayList<>();

        when(authorService.getAll()).thenReturn(emptyAuthorList);

        mockMvc.perform(MockMvcRequestBuilders.get(END_POINT_PATH).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent())
                .andDo(print());
    }

    @Test
    public void testGetAllAuthorsReturnOk200() throws Exception {

        AuthorRespDTO authorRespDTO = AuthorRespDTO.builder()
                .id(1L)
                .name("author name")
                .build();

        List<AuthorRespDTO> returnAuthorList = new ArrayList<>();
        returnAuthorList.add(authorRespDTO);

        when(authorService.getAll()).thenReturn(returnAuthorList);

        mockMvc.perform(MockMvcRequestBuilders.get(END_POINT_PATH).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(print());
    }
}
