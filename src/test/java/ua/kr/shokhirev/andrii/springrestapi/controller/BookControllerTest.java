package ua.kr.shokhirev.andrii.springrestapi.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import ua.kr.shokhirev.andrii.springrestapi.model.Author;
import ua.kr.shokhirev.andrii.springrestapi.model.Book;
import ua.kr.shokhirev.andrii.springrestapi.model.dto.AuthorRespDTO;
import ua.kr.shokhirev.andrii.springrestapi.model.dto.BookReqDTO;
import ua.kr.shokhirev.andrii.springrestapi.model.dto.BookRespDTO;
import ua.kr.shokhirev.andrii.springrestapi.model.mappers.AuthorMapper;
import ua.kr.shokhirev.andrii.springrestapi.model.mappers.BookMapper;
import ua.kr.shokhirev.andrii.springrestapi.service.BookService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@ExtendWith(SpringExtension.class)
public class BookControllerTest {

    private static String END_POINT_PATH = "/book-manager/books";

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private AuthorMapper authorMapper;

    @Autowired
    private BookMapper bookMapper;

    @MockBean
    private BookService bookService;

    @Test
    public void testSaveReturn400BadRequest() throws Exception {
        BookReqDTO bookReqDTO = BookReqDTO.builder().name("").authorId(null).genre("").publicationYear(null).build();

        String requestBody = objectMapper.writeValueAsString(bookReqDTO);

        when(bookService.saveBook(Mockito.any(BookReqDTO.class))).thenReturn(Optional.ofNullable(null));

        mockMvc.perform(post(END_POINT_PATH).contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody))
                        .andExpect(status().isBadRequest())
                        .andDo(print());
    }

    @Test
    public void testSaveReturn201Created() throws Exception {
        BookReqDTO bookReqDTO = BookReqDTO.builder()
                .authorId(1L)
                .genre("genre")
                .name("name")
                .publicationYear(2000L)
                .build();

        String requestBody = objectMapper.writeValueAsString(bookReqDTO);

        Optional<BookRespDTO> bookRespDTO = Optional.ofNullable(bookMapper.bookToBookResponse(
                bookMapper.getBookFromRequest(bookReqDTO)));

        when(bookService.saveBook(Mockito.any(BookReqDTO.class))).thenReturn(bookRespDTO);

        mockMvc.perform(post(END_POINT_PATH).contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody))
                        .andExpect(status().isCreated())
                        .andDo(print());
    }

    @Test
    public void testGetBookThatDoNotExistsReturn400BadRequest() throws Exception {
        Long bookId = 123L;
        String requestURI = END_POINT_PATH + "/" + bookId;

        mockMvc.perform(MockMvcRequestBuilders.get(requestURI).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andDo(print());
    }

    @Test
    public void testGetBookReturnOk200() throws Exception {

        Long bookId = 123L;
        String requestURI = END_POINT_PATH + "/" + bookId;

        BookReqDTO bookReqDTO = BookReqDTO.builder()
                .name("name")
                .publicationYear(2000L)
                .genre("genre")
                .authorId(1L)
                .build();

        Optional<BookRespDTO> bookRespDTOOptional = Optional.ofNullable(
                bookMapper.bookToBookResponse(bookMapper.getBookFromRequest(bookReqDTO)));

        when(bookService.getSingleBook(Mockito.any(Long.class))).thenReturn(bookRespDTOOptional);

        mockMvc.perform(MockMvcRequestBuilders.get(requestURI).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(print());
    }

    @Test
    public void testGetAllBooksReturn204NoContent() throws Exception {

        List<BookRespDTO> emptyBookList = new ArrayList<>();

        when(bookService.getBooks()).thenReturn(emptyBookList);

        mockMvc.perform(MockMvcRequestBuilders.get(END_POINT_PATH).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent())
                .andDo(print());
    }

    @Test
    public void testGetAllBooksReturnOk200() throws Exception {

        AuthorRespDTO authorRespDTO = AuthorRespDTO.builder()
                .id(1L)
                .name("author name")
                .build();

        BookRespDTO bookRespDTO = BookRespDTO.builder()
                .name("name")
                .publicationYear(2000L)
                .genre("genre")
                .id(1L)
                .author(authorRespDTO)
                .build();

        List<BookRespDTO> returnBookList = new ArrayList<>();
        returnBookList.add(bookRespDTO);

        when(bookService.getBooks()).thenReturn(returnBookList);

        mockMvc.perform(MockMvcRequestBuilders.get(END_POINT_PATH).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(print());
    }

    @Test
    public void testDeleteBookReturn204NoContent() throws Exception {

        Long bookId = 123L;
        String requestURI = END_POINT_PATH + "/delete/" + bookId;

        when(bookService.deleteBook(Mockito.any(Long.class))).thenReturn(Optional.ofNullable(null));

        mockMvc.perform(MockMvcRequestBuilders.delete(requestURI).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent())
                .andDo(print());
    }

    @Test
    public void testDeleteBookReturnOk200() throws Exception {

        Long bookId = 123L;
        String requestURI = END_POINT_PATH + "/delete/" + bookId;

        AuthorRespDTO authorRespDTO = AuthorRespDTO.builder()
                .id(1L)
                .name("author name")
                .build();

        BookRespDTO bookRespDTO = BookRespDTO.builder()
                .name("name")
                .publicationYear(2000L)
                .genre("genre")
                .id(1L)
                .author(authorRespDTO)
                .build();

        when(bookService.deleteBook(Mockito.any(Long.class))).thenReturn(Optional.ofNullable(bookRespDTO));

        mockMvc.perform(MockMvcRequestBuilders.delete(requestURI).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(print());
    }

    @Test
    public void testUpdateBookReturnsOk200() throws Exception {

        Long bookId = 123L;
        String requestURI = END_POINT_PATH + "/" + bookId;

        BookReqDTO bookReqDTO = BookReqDTO.builder()
                .authorId(1L)
                .genre("genre")
                .name("name")
                .publicationYear(2000L)
                .build();

        String requestBody = objectMapper.writeValueAsString(bookReqDTO);

        Optional<BookRespDTO> bookRespDTO = Optional.ofNullable(bookMapper.bookToBookResponse(
                bookMapper.getBookFromRequest(bookReqDTO)));

        when(bookService.updateBook(Mockito.any(Long.class), Mockito.any(BookReqDTO.class))).thenReturn(bookRespDTO);

        mockMvc.perform(MockMvcRequestBuilders.put(requestURI).contentType(MediaType.APPLICATION_JSON)
                .content(requestBody))
                .andExpect(status().isOk())
                .andDo(print());
    }

    @Test
    public void testUpdateBookReturns404BadRequest() throws Exception {

        Long bookId = 123L;
        String requestURI = END_POINT_PATH + "/" + bookId;

        BookReqDTO bookReqDTO = BookReqDTO.builder()
                .authorId(1L)
                .genre("genre")
                .name("name")
                .publicationYear(2000L)
                .build();

        String requestBody = objectMapper.writeValueAsString(bookReqDTO);

        when(bookService.updateBook(Mockito.any(Long.class), Mockito.any(BookReqDTO.class))).thenReturn(Optional.ofNullable(null));

        mockMvc.perform(MockMvcRequestBuilders.put(requestURI).contentType(MediaType.APPLICATION_JSON)
                .content(requestBody))
                .andExpect(status().isBadRequest())
                .andDo(print());
    }

    @Test
    public void testGetAllBooksByAuthorIdReturn204NoContent() throws Exception {

        Long authorId = 123L;
        String requestURI = END_POINT_PATH + "/author/" + authorId;

        List<BookRespDTO> emptyBookList = new ArrayList<>();

        when(bookService.readByAuthorId(Mockito.any(Long.class))).thenReturn(emptyBookList);

        mockMvc.perform(MockMvcRequestBuilders.get(requestURI).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent())
                .andDo(print());
    }

    @Test
    public void testGetAllBooksByAuthorIdReturnOk200() throws Exception {

        Long authorId = 123L;
        String requestURI = END_POINT_PATH + "/author/" + authorId;

        AuthorRespDTO authorRespDTO = AuthorRespDTO.builder()
                .id(1L)
                .name("author name")
                .build();

        BookRespDTO bookRespDTO = BookRespDTO.builder()
                .name("name")
                .publicationYear(2000L)
                .genre("genre")
                .id(1L)
                .author(authorRespDTO)
                .build();

        List<BookRespDTO> returnBookList = new ArrayList<>();
        returnBookList.add(bookRespDTO);

        when(bookService.readByAuthorId(Mockito.any(Long.class))).thenReturn(returnBookList);

        mockMvc.perform(MockMvcRequestBuilders.get(requestURI).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(print());
    }
}
