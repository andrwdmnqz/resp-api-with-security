package ua.kr.shokhirev.andrii.springrestapi.repository;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import ua.kr.shokhirev.andrii.springrestapi.model.Author;
import ua.kr.shokhirev.andrii.springrestapi.model.Book;

import java.util.List;

@DataJpaTest
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
public class BookRepositoryTest {

    @Autowired
    private BookRepository bookRepository;

    @Autowired
    private AuthorRepository authorRepository;

    @Test
    public void bookRepositoryReturnsSavedBookTest() {

        Book book = Book.builder()
                .name("book")
                .author(Author.builder().build())
                .genre("genre")
                .publicationYear(1L).build();

        Book savedBook = bookRepository.save(book);

        Assertions.assertThat(savedBook).isNotNull();
        Assertions.assertThat(savedBook.getId()).isGreaterThan(0L);
    }

    @Test
    public void bookRepositoryGetAllBooksTest() {
        Author author = Author.builder()
                .name("author name")
                .build();

        authorRepository.save(author);

        Book book = Book.builder()
                .name("book")
                .author(author)
                .genre("genre")
                .publicationYear(1L).build();

        Book book2 = Book.builder()
                .name("book2")
                .author(author)
                .genre("genre2")
                .publicationYear(2L).build();

        bookRepository.save(book);
        bookRepository.save(book2);

        List<Book> bookList = bookRepository.findAllBooks();

        Assertions.assertThat(bookList).isNotNull();
        Assertions.assertThat(bookList.size()).isEqualTo(2);
    }

    @Test
    public void bookRepositoryGetBookByIdTest() {
        Author author = Author.builder()
                .name("author name")
                .build();

        authorRepository.save(author);

        Book book = Book.builder()
                .name("book")
                .author(author)
                .genre("genre")
                .publicationYear(1L).build();

        bookRepository.save(book);

        Book returnBook = bookRepository.getBookById(book.getId());

        Assertions.assertThat(returnBook).isNotNull();
    }

    @Test
    public void bookRepositoryUpdateBookTest() {
        Author author = Author.builder()
                .name("author name")
                .build();

        authorRepository.save(author);

        Book book = Book.builder()
                .name("book")
                .author(author)
                .genre("genre")
                .publicationYear(1L).build();

        bookRepository.save(book);

        book.setName("new book");

        bookRepository.save(book);

        Book updatedBook = bookRepository.getBookById(book.getId());

        Assertions.assertThat(updatedBook.getId()).isGreaterThan(0);
        Assertions.assertThat(updatedBook.getName()).isEqualTo(book.getName());
    }

    @Test
    public void bookRepositoryDeleteBookTest() {
        Author author = Author.builder()
                .name("author name")
                .build();

        authorRepository.save(author);

        Book book = Book.builder()
                .name("book")
                .author(author)
                .genre("genre")
                .publicationYear(1L).build();

        bookRepository.save(book);

        Integer updatedStatus = bookRepository.deleteBookById(book.getId());

        Book updatedBook = bookRepository.getBookById(book.getId());

        Assertions.assertThat(updatedStatus).isGreaterThan(0);
        Assertions.assertThat(updatedBook).isEqualTo(null);
    }

    @Test
    public void findByAuthorIdReturnsBookTest() {
        Author author = Author.builder()
                .name("author name")
                .build();

        Author author2 = Author.builder()
                .name("author name")
                .build();

        authorRepository.save(author);
        authorRepository.save(author2);

        Book book = Book.builder()
                .name("book")
                .author(author)
                .genre("genre")
                .publicationYear(1L).build();

        Book book2 = Book.builder()
                .name("book")
                .author(author)
                .genre("genre")
                .publicationYear(1L).build();

        Book book3 = Book.builder()
                .name("book")
                .author(author2)
                .genre("genre")
                .publicationYear(1L).build();

        bookRepository.save(book);
        bookRepository.save(book2);
        bookRepository.save(book3);

        List<Book> booksByAuthorId = bookRepository.findByAuthorId(author.getId());

        Assertions.assertThat(booksByAuthorId).isNotNull();
        Assertions.assertThat(booksByAuthorId.size()).isEqualTo(2);
    }
}
