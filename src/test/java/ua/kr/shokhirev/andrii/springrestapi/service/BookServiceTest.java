package ua.kr.shokhirev.andrii.springrestapi.service;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import ua.kr.shokhirev.andrii.springrestapi.model.Author;
import ua.kr.shokhirev.andrii.springrestapi.model.Book;
import ua.kr.shokhirev.andrii.springrestapi.model.dto.AuthorRespDTO;
import ua.kr.shokhirev.andrii.springrestapi.model.dto.BookReqDTO;
import ua.kr.shokhirev.andrii.springrestapi.model.dto.BookRespDTO;
import ua.kr.shokhirev.andrii.springrestapi.model.mappers.BookMapper;
import ua.kr.shokhirev.andrii.springrestapi.repository.AuthorRepository;
import ua.kr.shokhirev.andrii.springrestapi.repository.BookRepository;

import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class BookServiceTest {

    @Mock
    private BookRepository bookRepository;

    @Mock
    private AuthorRepository authorRepository;

    @Mock
    private BookMapper bookMapper;

    @InjectMocks
    private BookService bookService;

    @Test
    public void bookServiceSaveBookTest() {

        Author author = Author.builder()
                .id(1L)
                .name("author name")
                .build();

        when(authorRepository.save(Mockito.any(Author.class))).thenReturn(author);

        author = authorRepository.save(author);

        BookReqDTO bookReqDTO = BookReqDTO.builder()
                .authorId(author.getId())
                .genre("genre")
                .name("book")
                .publicationYear(1L)
                .build();

        Book book = Book.builder()
                .author(author)
                .publicationYear(1L)
                .genre("genre")
                .name("name")
                .build();

        when(bookMapper.getBookFromRequest(Mockito.any(BookReqDTO.class))).thenReturn(book);
        when(bookRepository.save(Mockito.any(Book.class))).thenReturn(book);

        Optional<BookRespDTO> bookRespDTO = bookService.saveBook(bookReqDTO);

        Assertions.assertThat(bookRespDTO).isNotNull();
    }

    @Test
    public void bookServiceGetAllBooksTest() {
        List<Book> bookList = Mockito.mock(List.class);

        when(bookRepository.findAllBooks()).thenReturn(bookList);

        List<BookRespDTO> requestBookList = bookService.getBooks();

        Assertions.assertThat(requestBookList).isNotNull();
    }

    @Test
    public void bookServiceGetBookByIdTest() {
        Book book = Mockito.mock(Book.class);

        when(bookRepository.getBookById(Mockito.any(Long.class))).thenReturn(book);

        Optional<BookRespDTO> requestBook = bookService.getSingleBook(1L);

        Assertions.assertThat(requestBook).isNotNull();
    }

    @Test
    public void bookServiceUpdateBookTest() {

        Author author = Author.builder()
                .id(1L)
                .name("author name")
                .build();

        when(authorRepository.save(Mockito.any(Author.class))).thenReturn(author);

        author = authorRepository.save(author);

        BookReqDTO bookReqDTO = BookReqDTO.builder()
                .authorId(author.getId())
                .genre("genre")
                .name("book")
                .publicationYear(1L)
                .build();

        Book book = Book.builder()
                .author(author)
                .publicationYear(1L)
                .genre("genre")
                .name("name")
                .build();

        when(bookMapper.getBookFromRequest(Mockito.any(BookReqDTO.class))).thenReturn(book);
        when(bookRepository.save(Mockito.any(Book.class))).thenReturn(book);

        book.setName("new name");

        Optional<BookRespDTO> bookRespDTO = bookService.updateBook(author.getId(), bookReqDTO);

        Assertions.assertThat(bookRespDTO).isNotNull();
        Assertions.assertThat(bookRespDTO.isPresent());
    }

    @Test
    public void bookServiceDeleteBookTest() {

        Author author = Author.builder()
                .name("author name")
                .id(1L)
                .build();

        Book book = Book.builder()
                .id(1L)
                .name("book")
                .author(author)
                .genre("genre")
                .publicationYear(1L).build();

        AuthorRespDTO authorRespDTO = AuthorRespDTO.builder()
                .name("author name")
                .id(1L)
                .build();

        BookRespDTO deleteResponse = BookRespDTO.builder()
                .author(authorRespDTO)
                .name("name")
                .publicationYear(1L)
                .genre("genre")
                .id(1L)
                .build();

        when(bookRepository.save(Mockito.any(Book.class))).thenReturn(book);
        when(bookMapper.bookToBookResponse(Mockito.any(Book.class))).thenReturn(deleteResponse);
        when(bookRepository.getBookById(Mockito.any(Long.class))).thenReturn(book);

        Book savedBook = bookRepository.save(book);
        Optional<BookRespDTO> bookRespDTO = bookService.deleteBook(book.getId());

        Assertions.assertThat(bookRespDTO).isNotNull();
    }

    @Test
    public void readByAuthorIdTest() {

        AuthorRespDTO authorRespDTO = AuthorRespDTO.builder()
                .id(1L)
                .name("author name")
                .build();

        BookRespDTO bookRespDTO = BookRespDTO.builder()
                .id(1L)
                .genre("genre")
                .author(authorRespDTO)
                .publicationYear(1L)
                .name("name")
                .build();

        List<Book> bookList = Mockito.mock(List.class);

        when(bookRepository.findByAuthorId(Mockito.any(Long.class))).thenReturn(bookList);

        List<BookRespDTO> bookRespDTOList = bookService.readByAuthorId(1L);

        Assertions.assertThat(bookRespDTOList).isNotNull();
    }
}
