package ua.kr.shokhirev.andrii.springrestapi.repository;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import ua.kr.shokhirev.andrii.springrestapi.model.Author;

import java.util.List;

@DataJpaTest
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
public class AuthorRepositoryTest {

    @Autowired
    private AuthorRepository authorRepository;

    @Test
    public void authorRepositorySaveReturnsSavedAuthor() {
        Author author = Author.builder()
                .name("author name")
                .build();

        Author savedAuthor = authorRepository.save(author);

        Assertions.assertThat(savedAuthor).isNotNull();
        Assertions.assertThat(savedAuthor.getId()).isGreaterThan(0L);
    }

    @Test
    public void authorRepositoryFindAllAuthorsTest() {
        Author author = Author.builder()
                .name("author name")
                .build();

        Author author2 = Author.builder()
                .name("author2 name")
                .build();

        authorRepository.save(author);
        authorRepository.save(author2);

        List<Author> savedAuthorsList = authorRepository.findAllAuthors();

        Assertions.assertThat(savedAuthorsList.size()).isNotNull();
        Assertions.assertThat(savedAuthorsList.size()).isGreaterThan(0);
    }

    @Test
    public void authorRepositoryGetAuthorByIdTest() {
        Author author = Author.builder()
                .name("author name")
                .build();

        authorRepository.save(author);

        Author savedAuthor = authorRepository.getAuthorById(author.getId());

        Assertions.assertThat(savedAuthor).isNotNull();
        Assertions.assertThat(savedAuthor.getId()).isEqualTo(author.getId());
    }
}
