package ua.kr.shokhirev.andrii.springrestapi.service;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import ua.kr.shokhirev.andrii.springrestapi.model.Author;
import ua.kr.shokhirev.andrii.springrestapi.model.dto.AuthorRespDTO;
import ua.kr.shokhirev.andrii.springrestapi.model.mappers.AuthorMapper;
import ua.kr.shokhirev.andrii.springrestapi.model.mappers.BookMapper;
import ua.kr.shokhirev.andrii.springrestapi.repository.AuthorRepository;
import ua.kr.shokhirev.andrii.springrestapi.repository.BookRepository;

import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class AuthorServiceTest {

    @Mock
    private AuthorRepository authorRepository;

    @Mock
    private AuthorMapper authorMapper;

    @InjectMocks
    private AuthorService authorService;

    @Test
    public void getAllAuthorsReturnAuthorRespDTOListTest() {
        List<Author> authorList = Mockito.mock(List.class);

        when(authorRepository.findAllAuthors()).thenReturn(authorList);

        List<AuthorRespDTO> authorRespDTOList = authorService.getAll();

        Assertions.assertThat(authorRespDTOList).isNotNull();
    }

    @Test
    public void getSingleAuthorReturnAuthorTest() {

        Author author = Author.builder()
                .id(1L)
                .name("author name")
                .build();

        AuthorRespDTO returnAuthorResp = AuthorRespDTO.builder()
                .id(1L)
                .name("author name")
                .build();

        when(authorRepository.getAuthorById(Mockito.any(Long.class))).thenReturn(author);
        when(authorMapper.getAuthorResponseFromAuthor(Mockito.any(Author.class))).thenReturn(returnAuthorResp);

        Optional<AuthorRespDTO> authorRespDTO = authorService.getSingleAuthor(author.getId());

        Assertions.assertThat(authorRespDTO.isPresent());
        Assertions.assertThat(authorRespDTO).isNotNull();
    }
}
