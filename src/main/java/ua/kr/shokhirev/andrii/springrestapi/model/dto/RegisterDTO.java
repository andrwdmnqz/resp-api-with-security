package ua.kr.shokhirev.andrii.springrestapi.model.dto;

import jakarta.validation.constraints.*;
import lombok.Data;

@Data
public class RegisterDTO {

    @NotBlank(message = "Username should not be blank or null!")
    private String username;

    @Size(min = 4, message = "Password is too short!")
    @Size(max = 20, message = "Password is too long!")
    @Pattern(regexp = "^[a-zA-Z0-9!@#$%^&*()_-]+$", message = "Password should contain only letters, digits and symbols!")
    private String password;

    @Email(message = "Email is not valid", regexp = "^[a-zA-Z0-9_!#$%&'*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$")
    @NotEmpty(message = "Email cannot be empty")
    private String email;
}
