package ua.kr.shokhirev.andrii.springrestapi.model.dto;

import lombok.Data;

@Data
public class AuthRespDTO {
    private String accessToken;
    private String tokenType = "Bearer ";

    public AuthRespDTO(String accessToken) {
        this.accessToken = accessToken;
    }
}
