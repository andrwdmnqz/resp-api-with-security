package ua.kr.shokhirev.andrii.springrestapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ua.kr.shokhirev.andrii.springrestapi.model.UserEntity;

import java.util.Optional;

public interface UserRepository extends JpaRepository<UserEntity, Long> {

    Optional<UserEntity> findByEmail(String username);

    Boolean existsByUsername(String username);
    Boolean existsByEmail(String email);
}
