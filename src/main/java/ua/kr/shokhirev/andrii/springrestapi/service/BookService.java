package ua.kr.shokhirev.andrii.springrestapi.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.kr.shokhirev.andrii.springrestapi.model.Book;
import ua.kr.shokhirev.andrii.springrestapi.model.dto.BookReqDTO;
import ua.kr.shokhirev.andrii.springrestapi.model.dto.BookRespDTO;
import ua.kr.shokhirev.andrii.springrestapi.model.mappers.BookMapper;
import ua.kr.shokhirev.andrii.springrestapi.repository.BookRepository;

import java.util.List;
import java.util.Optional;

@Service
public class BookService {

    @Autowired
    private BookRepository bookRepository;

    @Autowired
    private BookMapper bookMapper;

    public List<BookRespDTO> getBooks() {

        List<Book> allBooksList = bookRepository.findAll();

        return allBooksList.stream().map(bookMapper::bookToBookResponse).toList();
    }

    public Optional<BookRespDTO> getSingleBook(Long id) {

        return Optional.ofNullable(bookMapper.bookToBookResponse(bookRepository.findById(id).get()));
    }

    public Optional<BookRespDTO> saveBook(BookReqDTO bookReqDTO) {

        return Optional.ofNullable(bookMapper.bookToBookResponse(
                bookRepository.save(bookMapper.getBookFromRequest(bookReqDTO))));
    }

    public List<BookRespDTO> readByAuthorId(Long id) {

        List<Book> booksByAuthorList = bookRepository.findByAuthorId(id);

        return booksByAuthorList.stream().map(bookMapper::bookToBookResponse).toList();
    }

    public Optional<BookRespDTO> updateBook(Long id, BookReqDTO bookReqDTO) {

        Book bookToSave = bookMapper.getBookFromRequest(bookReqDTO);
        bookToSave.setId(id);

        return Optional.ofNullable(bookMapper.bookToBookResponse(bookRepository.save(bookToSave)));
    }

    public Optional<BookRespDTO> deleteBook(Long id) {
        Optional<BookRespDTO> bookRespDTO = Optional.ofNullable(bookMapper.bookToBookResponse(bookRepository.findById(id).get()));

        if (bookRespDTO.isPresent()) {
            bookRepository.deleteById(id);
        }

        return bookRespDTO;
    }
}
