package ua.kr.shokhirev.andrii.springrestapi.security;

public class SecurityConstants {
    public static final long JWT_EXPIRATION = 700000;
    public static final String JWT_TOKEN_CONSTANT = "jwt_token_constant";
}
