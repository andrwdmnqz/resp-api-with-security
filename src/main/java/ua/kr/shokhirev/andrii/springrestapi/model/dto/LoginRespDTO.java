package ua.kr.shokhirev.andrii.springrestapi.model.dto;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class LoginRespDTO {

    private String accessToken;
    private final String tokenType = "Bearer";
}
