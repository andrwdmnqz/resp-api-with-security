package ua.kr.shokhirev.andrii.springrestapi.controller;

import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ua.kr.shokhirev.andrii.springrestapi.model.dto.BookReqDTO;
import ua.kr.shokhirev.andrii.springrestapi.model.dto.BookRespDTO;
import ua.kr.shokhirev.andrii.springrestapi.service.BookService;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/book-manager")
public class BookController {

    @Autowired
    private BookService bookService;

    @GetMapping("/books")
    public ResponseEntity<List<BookRespDTO>> getBooks() {

        List<BookRespDTO> booksList;
        try {
            booksList = bookService.getBooks();
            if (booksList.isEmpty()) {
                throw new RuntimeException();
            }
        } catch (RuntimeException e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<BookRespDTO>>(booksList, HttpStatus.OK);
    }

    @GetMapping("/books/{id}")
    public ResponseEntity<BookRespDTO> getBook(@PathVariable("id") Long id) {

        Optional<BookRespDTO> book = bookService.getSingleBook(id);

        if (book.isPresent()) {
            return new ResponseEntity<BookRespDTO>(book.get(), HttpStatus.OK);
        }

        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @GetMapping("/books/author/{id}")
    public ResponseEntity<List<BookRespDTO>> getBooksByAuthorId(@PathVariable Long id) {

        List<BookRespDTO> bookRespDTOList;

        try {
            bookRespDTOList = bookService.readByAuthorId(id);
            if (bookRespDTOList.isEmpty()) {
                throw new RuntimeException();
            }
        } catch (RuntimeException e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<BookRespDTO>>(bookRespDTOList, HttpStatus.OK);
    }

    @PostMapping("/books")
    public ResponseEntity<BookRespDTO> saveBook(@Valid @RequestBody BookReqDTO book) {

        Optional<BookRespDTO> savedBook = bookService.saveBook(book);

        if (savedBook.isPresent()) {
            return new ResponseEntity<BookRespDTO>(savedBook.get(), HttpStatus.CREATED);
        }

        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @PutMapping("/books/{id}")
    public ResponseEntity<BookRespDTO> updateBook(@PathVariable Long id, @Valid @RequestBody BookReqDTO book) {

        Optional<BookRespDTO> resultOfUpdate = bookService.updateBook(id, book);

        if (resultOfUpdate.isPresent()) {
            return ResponseEntity.status(HttpStatus.OK).body(resultOfUpdate.get());
        }

        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @DeleteMapping("/books/delete/{id}")
    public ResponseEntity<BookRespDTO> deleteBook(@PathVariable Long id) {

        Optional<BookRespDTO> deletedBook = bookService.deleteBook(id);
        if (deletedBook.isPresent()) {
            return ResponseEntity.status(HttpStatus.OK).body(deletedBook.get());
        }

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
