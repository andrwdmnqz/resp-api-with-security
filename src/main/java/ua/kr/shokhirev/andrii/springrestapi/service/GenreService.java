package ua.kr.shokhirev.andrii.springrestapi.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.kr.shokhirev.andrii.springrestapi.model.dto.GenreRespDTO;
import ua.kr.shokhirev.andrii.springrestapi.model.mappers.GenreMapper;
import ua.kr.shokhirev.andrii.springrestapi.repository.GenreRepository;

import java.util.List;
import java.util.Optional;

@Service
public class GenreService {

    @Autowired
    private GenreRepository genreRepository;

    @Autowired
    private GenreMapper genreMapper;

    public List<GenreRespDTO> getAll() {
        return genreRepository.findAll().stream().map(genreMapper::getGenreResponseFromGenre).toList();
    }

    public Optional<GenreRespDTO> getSingleGenre(Long id) {
        return Optional.ofNullable(genreMapper.getGenreResponseFromGenre(genreRepository.findById(id).get()));
    }
}
