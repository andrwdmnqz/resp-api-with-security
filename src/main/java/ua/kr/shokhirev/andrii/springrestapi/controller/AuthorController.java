package ua.kr.shokhirev.andrii.springrestapi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ua.kr.shokhirev.andrii.springrestapi.model.dto.AuthorRespDTO;
import ua.kr.shokhirev.andrii.springrestapi.model.dto.BookRespDTO;
import ua.kr.shokhirev.andrii.springrestapi.service.AuthorService;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/authors")
public class AuthorController {

    @Autowired
    private AuthorService authorService;

    @GetMapping
    public ResponseEntity<List<AuthorRespDTO>> getAuthors() {

        List<AuthorRespDTO> authorList;
        try {
            authorList = authorService.getAll();
            if (authorList.isEmpty()) {
                throw new RuntimeException();
            }
        } catch (RuntimeException e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<AuthorRespDTO>>(authorList, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<AuthorRespDTO> getBook(@PathVariable("id") Long id) {

        Optional<AuthorRespDTO> authorRespDTO = authorService.getSingleAuthor(id);

        if (authorRespDTO.isPresent()) {
            return new ResponseEntity<AuthorRespDTO>(authorRespDTO.get(), HttpStatus.OK);
        }

        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
}
