package ua.kr.shokhirev.andrii.springrestapi.controller;

import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ua.kr.shokhirev.andrii.springrestapi.model.dto.LoginReqDTO;
import ua.kr.shokhirev.andrii.springrestapi.model.dto.LoginRespDTO;
import ua.kr.shokhirev.andrii.springrestapi.model.dto.RegisterDTO;
import ua.kr.shokhirev.andrii.springrestapi.security.JwtIssuer;
import ua.kr.shokhirev.andrii.springrestapi.security.UserPrincipal;
import ua.kr.shokhirev.andrii.springrestapi.service.UserService;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/auth")
public class AuthController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserService userService;

    @Autowired
    private JwtIssuer jwtIssuer;

    @PostMapping("/login")
    public ResponseEntity<LoginRespDTO> login(@Valid @RequestBody LoginReqDTO loginReqDTO) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginReqDTO.getEmail(), loginReqDTO.getPassword())
        );

        SecurityContextHolder.getContext().setAuthentication(authentication);

        UserPrincipal principal = (UserPrincipal) authentication.getPrincipal();

        List<String> roles = principal.getAuthorities()
                .stream()
                .map(GrantedAuthority::getAuthority)
                .toList();

        String token = jwtIssuer.issue(principal.getUserId(), principal.getEmail(), roles);

        LoginRespDTO response = LoginRespDTO.builder()
                .accessToken(token)
                .build();

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PostMapping("/register")
    public ResponseEntity<String> register(@Valid @RequestBody RegisterDTO registerDTO) {

        String registeredStatus = userService.registerUser(registerDTO);

        if (registeredStatus.equals("User registered successfully!")) {
            return new ResponseEntity<String>(registeredStatus, HttpStatus.OK);
        }

        return new ResponseEntity<>(registeredStatus, HttpStatus.BAD_REQUEST);
    }
}
