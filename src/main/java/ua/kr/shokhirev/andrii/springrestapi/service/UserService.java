package ua.kr.shokhirev.andrii.springrestapi.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ua.kr.shokhirev.andrii.springrestapi.model.Role;
import ua.kr.shokhirev.andrii.springrestapi.model.UserEntity;
import ua.kr.shokhirev.andrii.springrestapi.model.dto.RegisterDTO;
import ua.kr.shokhirev.andrii.springrestapi.model.dto.UserRespDTO;
import ua.kr.shokhirev.andrii.springrestapi.repository.RoleRepository;
import ua.kr.shokhirev.andrii.springrestapi.repository.UserRepository;

import java.util.Collections;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public Optional<UserRespDTO> findByEmail(String email) {

        Optional<UserEntity> userOptional = userRepository.findByEmail(email);

        if (userOptional.isPresent()) {
            return Optional.of(UserRespDTO.builder()
                    .id(userOptional.get().getId())
                    .email(userOptional.get().getEmail())
                    .roles(userOptional.get().getRoles())
                    .password(userOptional.get().getPassword())
                    .build());
        }
        return Optional.empty();
    }

    public String registerUser(RegisterDTO registerDTO) {
        if (userRepository.existsByUsername(registerDTO.getUsername())) {
            return "Username is taken!";
        }

        if (userRepository.existsByEmail(registerDTO.getEmail())) {
            return "Email is used!";
        }

        UserEntity user = new UserEntity();
        user.setUsername(registerDTO.getUsername());
        user.setEmail(registerDTO.getEmail());
        user.setPassword(passwordEncoder.encode(registerDTO.getPassword()));

        Role roles = roleRepository.findByName("ROLE_USER").get();
        user.setRoles(Collections.singletonList(roles));

        userRepository.save(user);

        return "User registered successfully!";
    }
}
