package ua.kr.shokhirev.andrii.springrestapi.model.mappers;

import org.springframework.stereotype.Component;
import ua.kr.shokhirev.andrii.springrestapi.model.Genre;
import ua.kr.shokhirev.andrii.springrestapi.model.dto.GenreRespDTO;

@Component
public class GenreMapper {

    public GenreRespDTO getGenreResponseFromGenre(Genre genre) {
        if (genre == null) {
            return null;
        }
        return GenreRespDTO.builder()
                .id(genre.getId())
                .name(genre.getName())
                .build();
    }

    public Genre genreResponseToGenre(GenreRespDTO genreRespDTO) {
        return Genre.builder()
                .id(genreRespDTO.getId())
                .name(genreRespDTO.getName())
                .build();
    }
}
