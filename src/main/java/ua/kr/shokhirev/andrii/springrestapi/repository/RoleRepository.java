package ua.kr.shokhirev.andrii.springrestapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ua.kr.shokhirev.andrii.springrestapi.model.Role;

import java.util.Optional;

public interface RoleRepository extends JpaRepository<Role, Long> {

    Optional<Role> findByName(String name);
}
