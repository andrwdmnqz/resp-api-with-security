package ua.kr.shokhirev.andrii.springrestapi.model.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Builder;
import lombok.Data;
import ua.kr.shokhirev.andrii.springrestapi.model.Role;

import java.util.List;

@Data
@Builder
public class UserRespDTO {

    private long id;

    private String email;

    private List<Role> roles;

    @JsonIgnore
    private String password;
}
