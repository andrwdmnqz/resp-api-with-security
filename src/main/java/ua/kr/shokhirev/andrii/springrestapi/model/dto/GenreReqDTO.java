package ua.kr.shokhirev.andrii.springrestapi.model.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class GenreReqDTO {

    private String name;
}
