package ua.kr.shokhirev.andrii.springrestapi.model.mappers;

import org.springframework.stereotype.Component;
import ua.kr.shokhirev.andrii.springrestapi.model.Author;
import ua.kr.shokhirev.andrii.springrestapi.model.dto.AuthorRespDTO;

@Component
public class AuthorMapper {

    public AuthorRespDTO getAuthorResponseFromAuthor(Author author) {
        if (author == null) {
            return null;
        }
        return AuthorRespDTO.builder()
                .id(author.getId())
                .name(author.getName())
                .build();
    }

    public Author authorResponseToAuthor(AuthorRespDTO authorRespDTO) {
        return Author.builder()
                .id(authorRespDTO.getId())
                .name(authorRespDTO.getName())
                .build();
    }
}
